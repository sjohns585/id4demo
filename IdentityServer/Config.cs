﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer
{
    public class Config
    {
        // User store (in-memory - normally this would be Azure AD, Google, Windows auth, your own sql server database, asp.net identity, etc)
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password",
                    ProviderName="config",

                    Claims = new List<Claim>
                    {
                        new Claim("name", "Alice"),
                        new Claim(JwtClaimTypes.Role, "Manager"),
                        new Claim(JwtClaimTypes.WebSite, "http://alice.com"),
                        new Claim(JwtClaimTypes.Email, "alice.smith@hotmail.com"),
                        new Claim(JwtClaimTypes.GivenName, "Alice"),
                        new Claim(JwtClaimTypes.FamilyName, "Smith"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean)
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password",
                    ProviderName="config",

                    Claims = new List<Claim>
                    {
                        new Claim("name", "Bob"),
                        new Claim(JwtClaimTypes.Role, "Consultant"),
                        new Claim(JwtClaimTypes.WebSite, "http://bobsconsulting.com"),
                        new Claim(JwtClaimTypes.Email, "bob.white@gmail.com"),
                        new Claim(JwtClaimTypes.GivenName, "Bob"),
                        new Claim(JwtClaimTypes.FamilyName, "White"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean)
                    }
                },
                new TestUser
                {
                    SubjectId = "3",
                    Username = "sherri",
                    Password = "password",
                    ProviderName="config",

                    Claims = new List<Claim>
                    {
                    new Claim(JwtClaimTypes.Name, "Sherri Johns"),
                    new Claim(JwtClaimTypes.Address, "585 Savannah Drive"),
                    new Claim(JwtClaimTypes.Role, "Employee"),
                    new Claim(JwtClaimTypes.WebSite, "http://sherri.com"),
                    new Claim(JwtClaimTypes.Email, "sherri.johns@gmail.com"),
                    new Claim(JwtClaimTypes.GivenName, "Sherri"),
                    new Claim(JwtClaimTypes.FamilyName, "Johns"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean)
                    }
                }

            };
        }

        /// <summary>
        //  Resources - define resources that we want to protect (web apis and identity information about our users)
        /// </summary>

        // First, apis that we need to limit access to
        // The web api doesn't actually need to be named "api1" - it does, however, need to identify itself as "api1" 
        // whenever it interacts with IdentityServer
        // More info here: https://identityserver4.readthedocs.io/en/release/reference/api_resource.html#refapiresource
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("api1", "My API")
            };
        }

        // Define what information about users is available
        // It's required that you provide a unique ID for your users (so you have to include IdentityResources.OpenId, at a minimum)
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };

            // You can also customize the IdentityResource class, e.g. if you want to define what user claims should be included in the
            // the identity token when this resource is requested
            // More info here: https://identityserver4.readthedocs.io/en/release/reference/identity_resource.html#refidentityresource
            var customProfile = new IdentityResource(
                   name: "custom.profile",
                   displayName: "Custom profile",
                   claimTypes: new[] { "name", "email", "status" });

            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                customProfile
            };


        }



        // Client who can request tokens to access the above resources
        // Details vary, but typically you have to define these settings:
        //
        // 1. A unique client ID
        // 2. A secret if needed
        // 3. The allowed interactions with the token service (called a grant type)
        // 4. A network location where identity and/or access token gets sent to (called a redirect URI)
        // 5. A list of scopes(aka resources) the client is allowed to access
        // More info here: https://identityserver4.readthedocs.io/en/release/topics/clients.html

        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {
                // OpenID Connect implicit flow client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    RedirectUris = { "http://localhost:5002/signin-oidc" },
                    PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "api1"
                    }
                }
            };
        }

   }

}

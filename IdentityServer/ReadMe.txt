﻿This is an example of how to set up Identity Server for hybrid flow authentication.

In the simplest QuickStart samples, OpenID Connect implicit flow is used. When implicit flow is used, all tokens are transmitted via the broswer 
(which is fine for identity tokens). However, when you need an access token (e.g. to access a protected api), the token is a bit more sensitive,
and it's considered more secure to retrieve it using a back channel to the token service. 

Hybrid flow: the identity token is transmitted via the browser channel, so the client can validate it. If validation is successful, the client
opens a back-channel to IdentityServer to retrieve the access token (which will be used in calling the protected api).

Most of the code for this project was downloaded from https://github.com/IdentityServer/IdentityServer4.Samples, and specifically, from 
sample 5, HybridFlowAuthenticationWithApiAccess.

Files which were customized:

Config.cs - to provide IdentityServer with information about our local users, apis, and clients.  In a real-world scenario, you wouldn't store this 
information in a config file; you'd need a persistent and secure data store (e.g. a database).

Startup.cs - to add IdentityServer resources to the DI system and configure the HTTP pipeline.
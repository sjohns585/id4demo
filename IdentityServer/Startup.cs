﻿using System;
using IdentityServer4;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your IdentityServer startup file, visit https://identityserver4.readthedocs.io/en/release/topics/startup.html
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            // Add IdentityServer services to the DI container, using a temporary signing key (for development). For production,
            // you'll need a signing certificate/private key (x509 cert), or raw RSA keys.
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()

            // configure identity server with in-memory stores, keys, clients and scopes. In a real-world scenario, you wouldn't store this 
            // information in a config file; you'd need a persistent and secure data store (e.g. a database).
                .AddTestUsers(Config.GetUsers())
                .AddInMemoryApiResources(Config.GetApiResources())
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryClients(Config.GetClients());

            // Add Authentication services to the DI system. First, Google Auth 
            services.AddAuthentication()
                .AddGoogle("Google", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.ClientId = "434483408261-55tc8n0cs4ff1fe21ea8df2o443v2iuc.apps.googleusercontent.com";
                    options.ClientSecret = "3gcoTrEDPPJ0ukn_aYYT6PWo";
                })

              // Add Slack 
              .AddSlack(options =>
              {
                  options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                  options.ClientId = "3703767189.384380849396";
                  options.ClientSecret = "9032bde8eadc3af8913763a350be08b9";
                  options.CallbackPath = "/signin-slack";
              });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // UseIdentityServer includes a call to UseAuthentication, so not necessary to have both.
            app.UseIdentityServer();

            // Be aware that order matters in the pipeline. For example, IdentityServer needs to be added before the
            // UI that implements the login screen.
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}

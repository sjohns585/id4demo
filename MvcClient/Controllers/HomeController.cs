﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace MvcClient.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Public()
        {
            ViewData["Message"] = "Public page.";

            return View();
        }

        [Authorize]
        //  [Authorize(Role = "Administrator")]
        public IActionResult Secure()
        {
            List<Claim> claims = HttpContext.User.Claims.ToList();
            ViewData["Message"] = "Secure page.";

            return View();
        }

        // Client is contacting IdentityServer (backchannel) with its own credentials to request an access token for the api
        public async Task<IActionResult> CallApiUsingClientCredentials()
        {
            var tokenClient = new TokenClient("http://localhost:5000/connect/token", "mvc", "secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api1");

            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);
            var content = await client.GetStringAsync("http://localhost:5001/values");

            ViewBag.Json = JArray.Parse(content).ToString();
            return View("json");
        }

        // Client is authenticating to the api, on behalf of the user, because it's using the access_token included with the user's claims
        public async Task<IActionResult> CallApiUsingUserAccessToken()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            var content = await client.GetStringAsync("http://localhost:5001/values");

            ViewBag.Json = JArray.Parse(content).ToString();
            return View("json");
        }

        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}

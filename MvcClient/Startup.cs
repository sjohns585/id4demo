﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MvcClient
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            // This is the standard .AddAuthentication used in .net core authentication (not specific to IdentityServer).
            // We need to specify that we want to use cookies/oidc to authenticate users (i.e. the application will attempt to authenticate the user 
            // using the existing cookie. If that fails (because there’s no cookie, or its expired), then an authentication challenge will be made using the OIDC scheme.
            // When the user logs in to IdentityServer, a cookie will be created, so on the next request, the cookie authentication scheme will be able to 
            // authenticate the user (without having to use oidc again). That's why we need the url of the IdentityServer though.
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            })
                .AddCookie("Cookies")
                .AddOpenIdConnect("oidc", options =>
                {
                    options.SignInScheme = "Cookies";

                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;

                    // We'll provide a client name and client secret, because we'll need it when we call an api using the client credentials flow. 
                    options.ClientId = "mvc";
                    options.ClientSecret = "secret";

                    // Response type = code id_token means that we are asking for both an auth code and an identity token
                    // This enables two types of flows: the client can use the identity token for its own authentication needs, and if the client calls a protected api,
                    // it can use the auth code to request an access token from IdentityServer (back-channel).
                    options.ResponseType = "code id_token";
                    options.SaveTokens = true;
                    options.GetClaimsFromUserInfoEndpoint = true;

                    // api1 = identifier of the protected web api that the mvc client will call. If permitted, the scope is embedded in the access token, 
                    // (IdentityServer will decide this, based on how its configured), so that the api will know that the client is allowed to access it. 
                    options.Scope.Add("api1");

                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
